# Playground for akademy-kde-org

## Build

Download the hugo extended binary in the [release page](https://github.com/gohugoio/hugo/releases). Unpack it and move the binary in you `$PATH`.

```
hugo serve # for developement
```

or

```
hugo
```
