---
title: Call for Participation
menu: ['2020']
---

[Akademy](/2020) is the annual KDE Community conference. If you are working on topics relevant to KDE, this is your chance to present your work and ideas to the KDE community at large. Akademy 2020 will take place online from Friday the 4th to Friday the 11th of September 2020. Training sessions will be held on Friday the 4th of September and the talks will be held on Saturday the 5th and Sunday the 6th of September. The rest of the week (Monday - Friday) will be Birds-of-a-Feather meetings (BoFs), unconference sessions and workshops.

If you think you have something interesting to present, please tell us about it. If you know of someone else who should present, please encourage them to do so too.

## What We Are Looking For

We are asking for talk proposals on topics relevant to the KDE Community and technology:

* Topics related to KDE's current [Community Goals](https://community.kde.org/Goals):
  * Consistency
  * All About the Apps
  * Wayland
* KDE In Action: use cases of KDE technology in real life; be it mobile, desktop deployments and so on
* Overview of what is going on in the various areas of the KDE community
* Collaboration between KDE and other Free Software projects
* Release, packaging, and distribution of software by KDE
* Increasing our reach through efforts such as accessibility, promotion, translation and localization
* Improving our governance and processes, community building

Don't let this list restrict your ideas though. You can submit a proposal even if it doesn't fit in this list of topics as long as it is relevant to KDE. To get an idea of talks that were accepted previously, check out the program from previous years: [2019](https://conf.kde.org/en/akademy2019/public/events), [2018](https://conf.kde.org/en/akademy2018/public/events), and [2017](https://conf.kde.org/en/akademy2017/public/events).

## What We Offer

Many creative and interested KDE, Qt and Free Software contributors and supporters — your attentive and receptive audience. An opportunity to present your application, share ideas and best practices, or gain new contributors. A cordial environment with people who want you to succeed. This is your chance to make a big splash.

Since the conference is online this year, we will be (live) streaming and rebroadcasting the talks and provide session moderation and Q&A support. Your presentation can be done from the comfort of your own home or office.

## Submission

Proposals require the following information:

* Title — the title of your session/presentation
* Abstract — a brief summary of your presentation
* Description — additional information not in the abstract; potential benefits of the topic for the audience
* A short bio — including anything that qualifies you to speak on your subject

**Please [submit](https://conf.kde.org/users/sign_in?conference_acronym=akademy2020&locale=en) your proposals by Sunday 14th June 2020 23:59 UTC**

Akademy attracts people from all over the world. For this reason, all talks are in English.

Not everyone has lots of experience in giving talks, we don't want this to put people off from submitting. If you would like help and advice with preparing your talk, please request this in the Private Notes section of your submission.

Your submissions can be in any of the following forms:

* Talks are 30 minutes (possible extension to 45 minutes)
* Panels are 30 minutes (possible extension to 45 minutes)
* Fast Track talks are 10 minutes long
* Lightning talks are 5 minutes long

Fast Track and Lightning talks are featured in a single track and do not include time for Q&A. Due to the online nature of this year's Akademy all time slots are encouraged not to exceed 1 hour, therefore Talks and Panels are encouraged to keep content under 30 minutes, leaving ample time for virtual, live Q&A with participants.

For panel discussions this year we would love to see some focused on topics related to KDE's three goals (Consistency, All About the Apps, and Wayland).

Workshops are included in the 2nd part of Akademy and can last for up to three hours.

Akademy has a lot of content so we will try hard to fit it into the schedule and make sure that presentations start and end on time.

If you think your presentation could work better with a time slot longer than the standard time slots given, please provide your reasons for this. Longer talks may also be turned into two or three sessions, a workshop, or special session later in the week, outside of the main conference tracks. In the case a workshop is best, we suggest a lightning-talk teaser to promote the workshop. You don't have to submit detailed proposals for the workshop parts yet. This will be done later. This call for participation focuses on the talks that take place during the weekend (4th & 5th of September).

Akademy is upbeat, but it is not frivolous. Help us see that you care about your topic, your presentation and your audience. Typos, sloppy or all-lowercase formatting and similar appearance oversights leave a bad impression and may count against your proposal. There's no need to overdo it. If it takes more than two paragraphs to get over the point of your topic, it's too much and should be slimmed down. The quicker you can make a good impression, both on the Program Committee and your audience, the better.


We are looking for originality. Akademy is intended to move KDE forward. Having the same people talking about the same things doesn't accomplish that goal. Thus, we favour original and novel content. If you have presented on a topic elsewhere, please add a new twist, new research, or recent development, something unique. Of course, if your talk is plain awesome as is, go for that.

Everyone submitting a proposal will be notified by the end of June, as to whether or not their proposal was accepted for the Akademy 2020 Program.

The Akademy Team will provide assistance to you with your talk and schedule a test-run to ensure your AV setup works. Help will also be available to provide you with suitable webcam/microphone etc to ensure a good experience for attendees.

All talks will be recorded and published on the Internet for free, along with a copy of the slide deck, live demo or anything else associated with the presentations. This benefits the larger KDE Community and those who can't make it to Akademy. You will retain full ownership of your slides and other materials, we request that you make your materials available under the CC-BY license.

Meet the Akademy 2020 Program Committee:

Aiswarya Kaitheri Kandoth, Niccolo Venerandi, Harald Sitter, Carl Schwan, Albert Astals Cid, Adriaan de Groot, Eike Hein, Bhavisha Dhruve & Nate Graham

If you have any questions you can email the [Program Committee](mailto:akademy-talks@kde.org).
