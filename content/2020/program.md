---
title: Program
menu: ['2020']
markup: mmark
---

{.table}
|Day     |           |
|:------:|:---------:|
|Fri 4th |Trainings  |
|Sat 5th |Tasks Day 1|
|Sun 6th |Talks Day 2|
|Mon 7th |BoFs, Workshops & Meetings - [KDE eV AGM](https://ev.kde.org/generalassembly/)|
|Tue 8th |BoFs, Workshops & Meetings|
|Wed 9th |BoFs, Workshops & Meetings|
|Thu 10th|BoFs, Workshops & Meetings|
|Fri 11th|BoFs, Workshops & Meetings|

